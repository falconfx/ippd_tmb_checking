![zzz.png](https://bitbucket.org/repo/x8eG446/images/2510122293-zzz.png)


[TOC]
## **Quiénes somos** ##
[->Ir a la página correspondiente para este apartado](https://bitbucket.org/falconfx/ippd_tmb_checking/wiki/Quienes%20Somos)

## **¿Qué es TMB - Tickets?** ##

[![Video.JPG](https://bitbucket.org/repo/x8eG446/images/3639405704-Video.JPG)](https://www.youtube.com/watch?v=y3NDM3BNxl0)

** *Haz clic en la imagen para que te lleve hacia el vídeo [Youtube]* **


TMB - Tickets es el futuro sobre la forma de pago del transporte público metropolitano de Barcelona. Actualmente el transporte público en la ciudad funciona con unas tarjetas físicas que compramos en puntos de venta físicos determinados. Disponemos de diferentes tipos de tarjetas, empezando desde las sencillas, en las que puedes hacer un solo viaje, hasta las que, durante un periodo de tiempo limitado puedes realizar el número de viajes que desees. Pues, ahora bien, con TMB tickets podemos comprar todos estos tipos de tarjetas con nuestro móvil, y desde el propio dispositivo poder utilizarlas y gestionarlas. Para el uso más cómodo de las tarjetas adquiridas mediante la aplicación, podremos conectar nuestro móvil con diferentes dispositivos que contentan NFC y de esta manera canjear los billetes con la app.
TMB tickets también te da la facilidad de poder comprar y gestionar las tarjetas a través de su página web http://tmb-viatges.tk , con lo que desde cualquier dispositivo que tenga acceso a internet se podrá interactuar con la página. 
Además, tiene de la posibilidad de consultar a tiempo real donde está situado el usuario mediante Google Maps y poder acceder a la consulta de horarios de los transportes deseados y ofrecidos por TMB.

###  **Historia del Proyecto** ###

TMB tickets surgió a partir de la idea de como solventar problemas habituales que una persona puede sufrir cuando viaja con transporte público. Basándonos en el sistema de tarjetas “Oyster” de Londres, donde el usuario recarga su tarjeta inteligente y paga con ella, surgió la idea de implementar un sistema parecido en un dispositivo que hoy en día prácticamente cualquier persona lleva encima: el móvil. ¿A quién no le ha pasado que alguna mañana se ha dejado la cartera en casa y dentro tenia la tarjeta de metro, y ha tenido que dar la vuelta? Pues con TMB tickets tenemos una opción más, ya que en la sociedad actual dónde la tecnología forma parte íntegra de nuestras vidas, el smartphone es una extensión más de nuestro cuerpo y nuestra ambición ha sido unificar diferentes tecnologias para que esta extensión sea capaz de dar beneficio a miles de personas a diario en una sola aplicación. 

## **¿A quién va dirigido?** ##

Esta nueva forma de compra y pago de viajes sobre el transporte publico esta orientado para todo usuario de la red de transporte público que disponga de acceso a Internet en su dispositivo móvil. El usuario puede acceder a esta aplicación o página web. Esta aplicación puede ser usada desde los más jóvenes hasta los más mayores, pues actualmente son muchos los poseedores de dicha tecnología. 
Pensándolo de otra forma no deja de ser un complemento de comodidad a las tarjetas físicas que se han utilizado hasta el momento.

## **¿Qué problema resuelve?** ##

La idea principal de TMB tickets es complementar a las tarjetas físicas que se han estado utilizando hasta ahora, pero los tiempos cambian y la tecnología avanza. Así pues, creemos que con la introducción de esta aplicación se facilitaría mucho la gestión y el uso de las tarjetas por parte de los usuarios. De esta forma los clientes pueden realizar una compra desde cualquier lugar con conexión a internet y en cualquier momento, evitando así que tengan que desplazarse hasta un punto de venta concreto. También facilitaría la gestión de dichas tarjetas y la posibilidad de compartirlas digitalmente con familiares y conocidos o devolver las tarjetas que ya no se usen.


## **¿Que principios de diseño contiene?** ##

- Patrón de diseño MVC
- Trabajo con metodologías ágiles
- Aplicación SOLID al código
- Aplicación de la ley Demeter
- Uso del principio de Hollywood
- Paralelización Multi-Threading
- .. y muchos más! 

## **Principios técnicos** ##
[->Ir a la página correspondiente para este apartado](https://bitbucket.org/falconfx/ippd_tmb_checking/wiki/Principios%20T%C3%A9cnicos)


## **¿Como se pretende mantener el proyecto para conseguir los objetivos?** ##

Pensamos que la mejor forma de mantener el proyecto a flote y conseguir todas las propuestas futuras, es usarlo mucho, contra más mejor! Pues actualmente esta en una fase de producto mínimo viable y lo que hace falta es testearlo, encontrando todo tipo de problemas o ciclos en los que pueda ser bueno mejorar el sistema.
Manteniendo sobretodo el principio de paralelización, podremos ayudar a que los objetivos cojan forma de la manera más optima posible y a su vez monitorizaremos los procesos tal que podamos encapsular al máximo cada funcionalidad de la infraestructura.