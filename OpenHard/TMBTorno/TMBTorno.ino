#include <Wire.h>
#include <PN532_I2C.h>
#include <PN532.h>
#include <NfcAdapter.h>
  
PN532_I2C pn532i2c(Wire);
PN532 nfc(pn532i2c);
int speakerPin = 12;

  
void setup(void) {
  Serial.begin(115200);
  Serial.println("Empezamos Programa");

  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("No he encontrado la placa");
    while (1); // halt
  }
  
  // Got ok data, print it out!
  Serial.print("He encontrado el chip"); Serial.println((versiondata>>24) & 0xFF, HEX); 
  Serial.print("Version del Firmware:"); Serial.print((versiondata>>16) & 0xFF, DEC); 
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
  

  nfc.setPassiveActivationRetries(0xFF);
  
  // configure board to read RFID tags
  nfc.SAMConfig();
    
  Serial.println("Esperando a una tarjeta válida");
}

void loop(void) {
  
  boolean success;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
  
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, &uid[0], &uidLength);
  
  if (success) {
    Serial.println("Encontrado!");
    Serial.print("Longitud UID: ");Serial.print(uidLength, DEC);Serial.println(" bytes");
    Serial.print("Valor UID: ");
    String hex_value = "";
    for (uint8_t i=0; i < uidLength; i++) 
    {
      Serial.print(" 0x");Serial.print(uid[i], HEX);       
      //Serial.print(" ");Serial.print(uid[i], HEX);       
      hex_value += (String)uid[i];
    }

    Serial.println(", value="+hex_value);

    if(hex_value == "124198213115") {
      tonoError();
      Serial.println("No puedes pasar. "); // Llave
    }
    else if(hex_value == "2507811746") {
      tonoError();
      Serial.println("No puedes pasar. "); //tarjeta
    }
    else if(hex_value == "1585232184") {
      tonoValido();
      Serial.println("Buen viaje!. ");
    }
    else if(hex_value == "8210226132")
    {
      tonoValido();
      Serial.println("Buen viaje!. ");
    }
    else
      //tonoError();
      //Serial.println("No reconozco la tarjeta.");

    Serial.println("");
    // Wait 1 second before continuing
    delay(1000);
  }
  else
  {
    // PN532 probably timed out waiting for a card
    Serial.println("Esperando usuario...");
  }
}


void tonoValido()
{
  for (int i = 0; i < 2; i++)
  {
    tone(speakerPin, HIGH);
    delay(20);
  }
  noTone(speakerPin);
  delay(200);
  for (int i = 0; i < 2; i++)
  {
    tone(speakerPin, HIGH);
    delay(20);
  }
   noTone(speakerPin);
}

void tonoError()
{
  for (int i = 0; i < 8; i++)
  {
    tone(speakerPin, HIGH);
    delay(20);
  }
  noTone(speakerPin);
  delay(100);
  for (int i = 0; i < 8; i++)
  {
    tone(speakerPin, HIGH);
    delay(20);
  }
  noTone(speakerPin);
  delay(100);
  for (int i = 0; i < 8; i++)
  {
    tone(speakerPin, HIGH);
    delay(20);
  }
  noTone(speakerPin);

}
