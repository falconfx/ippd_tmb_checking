<?php

namespace App\Http\Middleware;

use Closure;

class EntrustPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $user = \Auth::user();

        $route_name = $request->route()->getName();
        //dd($user->can($route_name));
        if($user->can($route_name)){
            return $next($request);
        }
        // Quitame
        return $next($request);

        abort(403);

    }
}
