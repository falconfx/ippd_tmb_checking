<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Stripe\Stripe;
use Stripe\Charge;
use Illuminate\Support\Facades\Redirect;
use Session;
use Exception;
use App\UsersTickets;
use App\User;
use App\StatusTickets;
use Carbon\Carbon;


class PaymentsController extends Controller
{
    
   public function quantityProduct(Request $request)
   {
    	$ticketId = $request->id;
    	$quantityTicket = $request->input('quantity'.$request->id);
      	$tickets = DB::table('tickets')->where('id', '=' , $ticketId)->get();
    	
    	return view('tickets.buy', ['tickets' => $tickets, 'quantity' => $quantityTicket]);
   }

   public function payment(Request $request)
   {
    	$quantity = $request->input('quantity');
    	$ticketId = $request->input('ticketId');
    	$amount = $request->input('price') * 100;
    	$source = $request->input( 'stripeToken');
    	Stripe::setApiKey( 'sk_test_7lm3XYSI4boAGJQIlkknNJ2W' );
    	$ticketBuyed = DB::table('tickets')->where('id', '=' , $ticketId)->first();
		$ownTicket = DB::table('users_tickets')->where('ticket_id', '=', $ticketId)
											   ->where('user_id', '=', Auth::user()->id)
											   ->first();

		//dd($ownTicket);
		if($ownTicket == null){
		
    	   $usersTickets = new UsersTickets();
		   $usersTickets->user_id = Auth::user()->id;
		   $usersTickets->ticket_id = $ticketId;
		   $usersTickets->quantity = $quantity;
		   $usersTickets->num_travels = $quantity * $ticketBuyed->available_travels;
		   $usersTickets->save();

		   //dd($usersTickets);


		   for($i=0; $i < $quantity; $i++)
		   {
		   		$statusTickets = new StatusTickets();
		   		$statusTickets->user_id = Auth::user()->id;
		   		$statusTickets->ticket_id = $ticketId;
		   		$statusTickets->num_travels = $ticketBuyed->available_travels;
		   		$statusTickets->status = 'active';
		   		$statusTickets->purchase_date = Carbon::now();
		   		$statusTickets->expiration_date = Carbon::now()->addDays($ticketBuyed->expiration_days); 
		   		$statusTickets->save();
		   }
		}
	  	else{
		  	$idOwnTicket = $ownTicket->id;
		  	$usersTickets = UsersTickets::find($idOwnTicket);
		  	$usersTickets->quantity += $quantity;
		  	$usersTickets->num_travels += $ticketBuyed->available_travels;
		  	$usersTickets->save();

		  	for($i=0; $i < $quantity; $i++)
		   {
		   		$statusTickets = new StatusTickets();
		   		$statusTickets->user_id = Auth::user()->id;
		   		$statusTickets->ticket_id = $ticketId;
		   		$statusTickets->num_travels = $ticketBuyed->available_travels;
		   		$statusTickets->status = 'active';
		   		$statusTickets->purchase_date = Carbon::now();
		   		$statusTickets->expiration_date = Carbon::now()->addDays($ticketBuyed->expiration_days); 
		   		$statusTickets->save();
		   }

		}

	   try 
	   {
	      Charge::create( 
	      	array (
					"amount" => $amount,
					"currency" => "eur",
					"source" => $source,
					"description" => "Ticket Payment" 
        		) 
	      );
		   
			Session::flash ( 'success-payment', 'Payment done successfully !' );
			return redirect()->route('tickets.list');
		} catch ( Exception $e ) 
		{
			Session::flash ( 'fail-payment', "Error! Please Try again." );
			return redirect()->route('tickets.list');
		}
	    
	}

	public function paymentWithoutLogin(Request $request)
	{
		$getUser = $request->input('user');
		$username = DB::table('users')->where('email', '=' , $getUser)->first();
		
		if(empty($username) == false)
		{
			Auth::loginUsingId($username->id);
			return redirect()->route('tickets.index');
		}
		else
		{
			dd("El usuario introducido no existe");
		}
	
 
	}
}
