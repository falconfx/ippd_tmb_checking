<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Buy;
use DB;
use Auth;
use Carbon\Carbon;

class TicketController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $tickets = DB::table('tickets')->get();
    	return view('tickets.index', ['tickets' => $tickets]);
    }

    public function buy($id)
    {
        $tickets = DB::table('tickets')->where('id', '=' , $id)->get();
        return view('tickets.buy', ['tickets' => $tickets]);

    }

    public function list()
    {
        
        $availableTickets = DB::table('tickets')
            ->join('users_tickets', 'users_tickets.ticket_id', '=', 'tickets.id')
            ->where('users_tickets.user_id', '=' , Auth::user()->id)
            ->select('tickets.name', 'tickets.description', 'tickets.color', 'users_tickets.quantity', 'users_tickets.num_travels', 'users_tickets.ticket_id')
            ->get();

           //dd($availableTickets);
        return view('tickets.list', ['availableTickets' => $availableTickets]);
    }

    public function info($ticket_id)
    {
        //dd($ticket_id);
        $infoTickets = DB::table('status_tickets')->where('ticket_id', '=', $ticket_id)->where('user_id', '=', Auth::user()->id)->orderByDesc('purchase_date')->get();
        $ticket = DB::table('tickets')->where('id', '=', $ticket_id)->first();
        $nameTicket = $ticket->name;
        //dd($infoTickets);
        return view('tickets.info', ['infoTickets' => $infoTickets, 'name' => $nameTicket]);

    }


    public function dashboard()
    {   

        $infoTickets = DB::table('status_tickets')->where('user_id', '=', Auth::user()->id)->orderByDesc('purchase_date')->get();
        $purchaseTickets = [];
        foreach ($infoTickets as $key => $infoT) {

            $ticket = DB::table('tickets')->where('id', '=', $infoT->ticket_id)->first();
            $nameTicket = $ticket->name;
            $parsedDate = Carbon::parse($infoT->purchase_date)->format('d-m-Y');
            $purchaseTickets[$parsedDate][]  = array('num_travels' => $infoT->num_travels, 'expiration_date' => $infoT->expiration_date, 'name' => $nameTicket, 'status' => $infoT->status, 'purchase_date' => $infoT->purchase_date);
            //var_dump($key);
        }

        //dd($purchaseTickets);

        return view('dashboard', ['purchaseTickets' => $purchaseTickets]);

    }

}