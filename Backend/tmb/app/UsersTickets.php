<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersTickets extends Model
{
    protected $table = 'users_tickets';
    protected $fillable = ['user_id','ticket_id','status_id','quantity','num_travels'];

}
