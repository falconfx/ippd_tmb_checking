<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusTickets extends Model
{
    protected $table = 'status_tickets';
    protected $fillable = [
        'user_id', 'ticket_id', 'status', 'purchase_date', 'expiration_date'
    ];

}

