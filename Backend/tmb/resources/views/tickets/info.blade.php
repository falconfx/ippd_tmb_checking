@extends('adminlte::layouts.app')

@section('contentheader_title')
   Información Tarjeta  <a href="{{ url('tickets/list') }}" style="text-decoration: none; color: #000"><i class="fa fa-arrow-circle-left"></i></a>

@endsection

@section('main-content')



<div class="container-fluid spark-screen">
	<div class="row">
	  <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Timeline de tu {{ $name }}</h3>

              <div class="box-tools">
              
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>Tarjeta</th>
                  <th>Número de Viajes</th>
                  
                  <th>Fecha de compra</th>
                  <th>Fecha de caducidad</th>
                  <th>Estado</th>
                </tr>
                @foreach($infoTickets as $infoTicket)
                <tr>
                  <td>{{$name}}</td>
                  <td>{{$infoTicket->num_travels}}</td>
                  @php
                     $purchase_date = $infoTicket->purchase_date;
                     $purchase_date_parse = \Carbon\Carbon::createFromTimestamp(strtotime($purchase_date))->format('l jS \\of F Y H:i:s A');  

                     $expiration_date = $infoTicket->expiration_date;
                     $expiration_date_parse = \Carbon\Carbon::createFromTimestamp(strtotime($expiration_date))->format('l jS \\of F Y H:i:s A');
                  @endphp
                  <td>{{$purchase_date_parse}}</td>
                  <td>{{$expiration_date_parse}}</td>
                  @php
                  if($infoTicket->status == 'active')
                  {
                  @endphp
                     <td><span class="badge bg-green">{{$infoTicket->status}}</span></td>
                  @php
                  }
                  else if($infoTicket->status == 'inactive')
                  {
                  @endphp
                     <td><span class="badge bg-red">{{$infoTicket->status}}</span></td>
                  @php
                  }
                  @endphp

                </tr>
                @endforeach
              </tbody></table>
            </div>
          </div>
        </div>
	</div>
</div>



@endsection