@extends('adminlte::layouts.app')

@section('contentheader_title')
   Mis tarjetas
@endsection

@section('main-content')



<div class="container-fluid spark-screen">
	<div class="row">
		@if(session()->has('success-payment'))
		   <div class="alert alert-success alert-dismissible">
  				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  				<strong>¡Genial!</strong> La compra se ha realizado con éxito .
			</div>
		@endif

		@if(session()->has('fail-payment'))
			<div class="alert alert-warning alert-dismissible">
  				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  				<strong>Lo sentimos.</strong> Pero ha habido un error al realizar la compra. Por favor, contacta con administración para resolver el problema.
			</div>
		@endif

      @php
      if(!$availableTickets->isEmpty()){

      @endphp

		@foreach($availableTickets as $availableTicket)
		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
         <div class="small-box bg-{{$availableTicket->color}}">
            <div class="inner">
               <h3>
                  [{{$availableTicket->quantity}}] {{$availableTicket->name}}
                  <a href="{!! route('tickets.info', ['ticket_id'=>$availableTicket->ticket_id]) !!}" type="button" style="padding: 0; border: none; background: none; text-decoration: none; color: #fff"> 
                    <i class="fa fa-info-circle" style="padding-right: 0" ></i>
                  </a> 
                  </h3> 
               <p>{{$availableTicket->description}}</p>
               <p>Dispones de {{$availableTicket->num_travels}} viajes en general
            </div>
            <div class="icon">
               <i class="fa fa-ticket" aria-hidden="true"></i>
            </div>
{{--
            <button type="submit" class="small-box-footer" style="padding: 0; border: none; background: none; padding-left: 40%">
                  <h4>
                     <b>Usar</b> 
                     <i class="fa fa-arrow-circle-right"></i>
                  </h4>
            </button>
 --}}

         </div>
      </div>
{{-- 

      <div class="modal fade" id="infoModal{{$availableTicket->id}}" tabindex="-1" role="dialog" aria-labelledby="infoModal{{$availableTicket->id}}">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" 
                  data-dismiss="modal" 
                  aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" 
                  id="favoritesModalLabel"><b>Información sobre tarjeta {{$availableTicket->name}}</b> </h4>
               </div>
               <div class="modal-body">
                  <p>El ticket {{$availableTicket->name}} es {{$availableTicket->description}}.</p>
                  <p>Dispones de <b>{{$availableTicket->currentTravels}} viajes</b> </p>
                  @php 
                     $expiration_date = $availableTicket->expiration_date;
                     $expiration_date_parse = \Carbon\Carbon::createFromTimestamp(strtotime($expiration_date))->format('l jS \\of F Y h:i:s A');  
                  @endphp
                  <p>Caduca el <b>{{$expiration_date_parse}}</b> </p>

               </div>
               <div class="modal-footer">
                  <button type="button" 
                  class="btn btn-default" 
                  data-dismiss="modal">Cerrar</button>
               </div>
            </div>
         </div>
      </div>
--}}
      @endforeach
      @php
      }else
      {
      @endphp
         <div class="alert alert-info alert-dismissible">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-info"></i> ¡Alerta!</h4>
            No dispones de ninguna tarjeta. Para usar nuestros servicios, primero debes <a href="{{ url('tickets/index') }}" style="text-decoration: none;"><b>comprar una tarjeta</b></a>.
         </div>
      @php
      }
      @endphp
	</div>
</div>



@endsection