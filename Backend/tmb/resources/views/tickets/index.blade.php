@extends('adminlte::layouts.app')

@section('contentheader_title')
   Comprar Viajes
@endsection

@section('main-content')

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<div class="container-fluid spark-screen">
   <div class="row">
      @foreach($tickets as $ticket)
      <form action="{{url('quantityProduct')}}" method="POST">
         {{ csrf_field() }}
         <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="small-box bg-{{ $ticket->color }}">
               <div class="inner">
                  <h3>{{ $ticket->name }}</h3>

                  <p>{{ $ticket->description }}</p>
               </div>
               <div class="icon">
                  <i class="fa fa-ticket" aria-hidden="true"></i>
               </div>
               <div class="input-group">
                  <span class="input-group-btn">
                     <button type="button" class="quantity-left-minus{{ $ticket->id }} btn btn-default btn-number"  data-type="minus" data-field="">
                        <span class="glyphicon glyphicon-minus"></span>
                     </button>
                  </span>
                  <input type="text" id="quantity{{ $ticket->id }}" name="quantity{{ $ticket->id }}" class="form-control input-number" value="1" min="1" max="10">
                  <span class="input-group-btn">
                     <button type="button" class="quantity-right-plus{{ $ticket->id }} btn btn-default btn-number" data-type="plus" data-field="">
                     <span class="glyphicon glyphicon-plus"></span>
                     </button>
                  </span>
               </div>
               <input type="hidden" name="id" value="{{$ticket->id}}">

               <button type="submit" class="small-box-footer" style="padding: 0; border: none; background: none; padding-left: 40%">Comprar 
                  <i class="fa fa-arrow-circle-right"></i>
               </button>
            </div>
         </div>
      </form>

      <script type="text/javascript">
         $(document).ready(function(){
            var quantitiy=1;

            $('.quantity-right-plus{{ $ticket->id }}').click(function(e)
            {
               e.preventDefault();
               var quantity = parseInt($('#quantity{{ $ticket->id }}').val());
               $('#quantity{{ $ticket->id }}').val(quantity +1); 
            });

            $('.quantity-left-minus{{ $ticket->id }}').click(function(e)
            {
               e.preventDefault();
               var quantity = parseInt($('#quantity{{ $ticket->id }}').val());
               if(quantity>1){
                  $('#quantity{{ $ticket->id }}').val(quantity - 1);
               }
            });

         });
      </script>
      
      @endforeach
   </div>
</div>
@endsection
