@extends('adminlte::layouts.app')

@section('contentheader_title')
  Comprar {{$tickets[0]->name}}

@endsection

@section('main-content')

@php 
	$finalPrice = $tickets[0]->price * $quantity;
@endphp


<div class="container-fluid spark-screen">
	<div class="row">
		<div class="box box-success">
	   	<div class="box-header with-border">
	      	<h3 class="box-title">Descripción del Producto</h3>
	    	</div>
	    	
	    	<div class="box-body">
		    	<div class="col-lg-3 col-md-6 col-xs-8">
			   	<div class="small-box bg-{{ $tickets[0]->color }}">
			   		<div class="inner">
					      <h3>{{ $tickets[0]->name }}</h3>
				         <p>{{ $tickets[0]->description }}</p>
				      </div>
					   <div class="icon">
					      <i class="fa fa-ticket" aria-hidden="true"></i>
					   </div>
				   </div>
				</div>
	      
		      <div class="col-lg-4 col-md-8 col-xs-12">
		         <p><h4>Has seleccionado {{ $quantity }} tarjetas del tipo {{ $tickets[0]->name }}, eso equivale a <b>{{ $finalPrice }} €</b>.</h4></p>
		         <form action="{{url('payment')}}" method="POST">	
         		{{ csrf_field() }}

		            <script
						   src="https://checkout.stripe.com/checkout.js" class="stripe-button"
						   data-key="{{config('services.stripe.key')}}"
						   data-amount="{{$finalPrice * 100}}"
						   data-name="UPF Projects"
						   data-description="Compra de {{ $tickets[0]->name }}"
						   data-image="https://lh3.googleusercontent.com/rxWH60baI--egv00WKL_Zps3MBZDPv9TPIeQbMe-d856RtdMuY3t_7ztjJRoZz9AtRE=s180-rw"
						   data-locale="auto"
						   data-currency="eur"
						   data-label="Pagar con Tarjeta"
						   data-email="{{\Auth::user()->email}}"
						   data-allow-remember-me="false" >
						</script>
					  	<input type="hidden" name="price" value="{{$finalPrice}}">
					  	<input type="hidden" name="email" value="{{\Auth::user()->email}}">
					  	<input type="hidden" name="ticketId" value="{{ $tickets[0]->id }}">
					  	<input type="hidden" name="quantity" value="{{ $quantity }}">


					</form>
				</div>
	   	</div>
	   </div>
	</div>
</div>


@endsection
