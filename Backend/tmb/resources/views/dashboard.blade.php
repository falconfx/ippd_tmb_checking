@extends('adminlte::layouts.app')

@section('contentheader_title')
  TMB
  
@endsection

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection



@section('main-content')

	<div class="container-fluid spark-screen">
		<div class="row">
      @php
      if(empty($purchaseTickets)){

      @endphp
       <div class="alert alert-info alert-dismissible">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-info"></i> ¡Alerta!</h4>
            No dispones de ninguna tarjeta. Para usar nuestros servicios, primero debes <a href="{{ url('tickets/index') }}" style="text-decoration: none;"><b>comprar una tarjeta</b></a>.
         </div>
      @php
      }else{
      @endphp
			<div class="col-md-12">
          <!-- The time line -->
          <ul class="timeline">
            <!-- timeline time label -->
            @foreach($purchaseTickets as $key => $purchaseTicket_)

            <li class="time-label">
                  <span class="bg-red">
                    {{$key}}
                  </span>
            </li>

            @foreach($purchaseTicket_ as $purchaseTicket)

             @php
    
               \Carbon\Carbon::setLocale('es');

               \Carbon\Carbon::setUtf8(false);

               $purchase_date = $purchaseTicket['purchase_date'];
               $purchase_hour_parse = \Carbon\Carbon::createFromTimestamp(strtotime($purchase_date))->format('H:i:s'); 
               

               $expiration_date = $purchaseTicket['expiration_date'];
               $expiration_date_parse = \Carbon\Carbon::createFromTimestamp(strtotime($expiration_date))->format('l jS \\of F Y h:i:s A'); 
               
            @endphp
            
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-ticket"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> {{$purchase_hour_parse}}</span>
                @php
                if($purchaseTicket['status'] == 'active'){ 
                @endphp
                <h3 class="timeline-header"><a href>Has comprado</a> una tarjeta {{$purchaseTicket['name']}}. La tarjeta está <span class="badge bg-green">activa</span></h3>
                @php
                }else if($purchaseTicket['status'] == 'inactive'){
                @endphp
                <h3 class="timeline-header"><a href>Has comprado</a> una tarjeta {{$purchaseTicket['name']}}. <i class="fa fa-exclamation-triangle"></i> La tarjeta está <span class="badge bg-red">inactiva</span> </h3>
                @php
                }
                @endphp

                <div class="timeline-body">
                  Has comprado una tarjeta {{$purchaseTicket['name']}} que caduca el <b>{{$expiration_date_parse}}</b>.
                  Todavía le quedan <b>{{$purchaseTicket['num_travels']}} viajes</b>.
                </div>
                <div class="timeline-footer">
                  
                  
                </div>
              </div>
            </li>
            
            @endforeach
            @endforeach
            
            
            
            <li>
              <i class="fa fa-clock-o bg-gray"></i>
            </li>
          </ul>
        </div>
      @php
      }
      @endphp
		</div>
	</div>
@endsection
