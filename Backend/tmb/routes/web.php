<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect('/login');
});


Route::get('/pay/{user?}',  ['as' => 'pay', 'uses' => 'PaymentsController@paymentWithoutLogin']);

Route::group(['middleware' =>  ['rolePermission', 'web']], function () {

    //Route::auth();
    //Route::get('/home', 'HomeController@index');
 
	Route::get('/home',  ['as' => 'home', 'uses' => 'TicketController@dashboard']);
	Route::post('quantityProduct',  ['as' => 'quantityProduct', 'uses' => 'PaymentsController@quantityProduct']);
	Route::post('payment', ['as' => 'payment', 'uses' => 'PaymentsController@payment']);

    Route::group(['prefix' => 'tickets', 'as' => 'tickets.'], function () {
	    Route::get('index', ['as' => 'index', 'uses' => 'TicketController@index']);
	    Route::get('{id}/buy', ['as' => 'buy', 'uses' => 'TicketController@buy']);
        Route::get('list', ['as' => 'list', 'uses'=>'TicketController@list']);
        Route::get('{ticket_id}/info', ['as' => 'info', 'uses'=>'TicketController@info']);

	});

});
