<?php

use Illuminate\Database\Seeder;
use App\Ticket;

class TicketTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tickets = [
            // TICKETS
            [
                'id' => 1,
                'name' => 'T-10',
                'description' => 'Válido para 10 viajes',
                'color' => 'aqua',
                'available_travels' => 10,
                'price' => 10,
                'expiration_days' => 365,
                'created_at' => '2018-05-24 00:00:00',
                'updated_at' => '2018-05-24 00:00:00'
            ],
            [
                'id' => 2,
                'name' => 'T-50',
                'description' => 'Válido para 50 viajes',
                'color' => 'green',
                'available_travels' => 50,
                'price' => 40,
                'expiration_days' => 30,
                'created_at' => '2018-05-24 00:00:00',
                'updated_at' => '2018-05-24 00:00:00'
            ]

        ];

        foreach ($tickets as $key => $value) {
            Ticket::create($value);
        }

    }
}
