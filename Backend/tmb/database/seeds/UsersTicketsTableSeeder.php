
<?php

use Illuminate\Database\Seeder;
use App\UsersTickets;

class UsersTicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users_tickets=[
          [
            'user_id' => 1,
            'ticket_id' => 1,
            'quantity' => 1,
            'num_travels'=> 10
          ],
          [
            'user_id' => 1,
            'ticket_id' => 2,
            'quantity' => 1,
            'num_travels'=> 50
          ]
        ];

        foreach ($users_tickets as $key => $value) {
            UsersTickets::create($value);
        }
    }
}
