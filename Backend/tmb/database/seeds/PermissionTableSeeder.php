<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = 
        [
            // DASHBOARD
            [
                'id' => 1,
                'name' => 'home',
                'display_name' => 'home',
                'description' => 'home'
            ],
            // TICKETS
            [
                'id' => 2,
                'name' => 'tickets',
                'display_name' => 'tickets',
                'description' => 'tickets'
            ],
            [
                'id' => 3,
                'name' => 'tickets.index',
                'display_name' => 'tickets.index',
                'description' => 'tickets.index'
            ],
            [
                'id' => 4,
                'name' => 'tickets.buy',
                'display_name' => 'tickets.buy',
                'description' => 'tickets.buy'
            ],
		];

        foreach ($permissions as $key => $value) {
            Permission::create($value);
        }
    }
}
