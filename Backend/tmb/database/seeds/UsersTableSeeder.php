<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = 
        [
          // Admin User
          [
              'id' => 1,
              'name' => 'Admin',
              'email' => 'admin@tmb.es', 
              'password' => '$2y$10$lF0bDwQSnHG7yi16jFihm.CVNM1c1ruyDNSBA.71ly3qyA4MQEYDy',
              'remember_token' => '',
              'created_at' => '2018-05-24 08:45:13',
              'updated_at' => '2018-05-24 08:45:13'
          ]

        ];

        foreach ($users as $key => $value) {
            User::create($value);
        }

    }
}
