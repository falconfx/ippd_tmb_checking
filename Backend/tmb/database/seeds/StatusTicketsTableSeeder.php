<?php

use Illuminate\Database\Seeder;
use App\StatusTickets;

class StatusTicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $status_tickets = [
            [
                'user_id' => 1,
                'ticket_id' => 1,
                'num_travels' => 10,
                'status' => 'active',
                'purchase_date' => '2018-05-24 00:00:00',
                'expiration_date' => '2019-05-24 00:00:00'
            ],
            [
                'user_id' => 1,
                'ticket_id' => 2,
                'num_travels' => 50,
                'status' => 'active',
                'purchase_date' => '2018-05-24 00:00:00',
                'expiration_date' => '2018-06-24 00:00:00'
            ]

        ];

        foreach ($status_tickets as $key => $value) {
            StatusTickets::create($value);
        }
    }
}

