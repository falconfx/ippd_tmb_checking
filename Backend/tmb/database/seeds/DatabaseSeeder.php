<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionTableSeeder::class);
      	$this->call(RoleTableSeeder::class);
      	$this->call(PermissionRoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(RoleUserTableSeeder::class);
        $this->call(TicketTableSeeder::class);
        $this->call(StatusTicketsTableSeeder::class);
        $this->call(UsersTicketsTableSeeder::class);
        


    }
}
