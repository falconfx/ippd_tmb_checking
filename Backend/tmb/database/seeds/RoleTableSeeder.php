<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles=[
	        [
	        	'id' => 1,
	        	'name' => 'administrator',
	        	'display_name' => 'Administrator',
	        	'description' => 'TMB Tickets Administrator'
	        ], 
	        [
	        	'id' => 2,
	        	'name' => 'user',
	        	'display_name' => 'User',
	        	'description' => 'TMB Tickets User'
	        ],

        ];


        foreach ($roles as $key => $value) {
        	Role::create($value);
        }
    }
}
