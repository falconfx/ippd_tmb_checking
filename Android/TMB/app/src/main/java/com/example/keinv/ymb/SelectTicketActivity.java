package com.example.keinv.ymb;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class SelectTicketActivity extends AppCompatActivity {

    String ticket_id, username;
    RecyclerView recyclerView;
    List<SelectListTicket> ticketsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_my_cards);

        recyclerView = (RecyclerView) findViewById(R.id.selectRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ticketsList = new ArrayList<>();

        Intent intent=getIntent();
        intent.getExtras();

        getExtraData();
        loadRecyclerViewData();
    }

    private void loadRecyclerViewData() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando Tarjetas...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://35.204.171.13/storage/selectTicket.php?name=" + username + "&ticket_id="+ticket_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //converting the string to json array object
                            JSONArray array = new JSONArray(response);

                            //traversing through all the object
                            Log.isLoggable("prueba", array.length() );
                            if(array.length() != 0){
                                for (int i = 0; i < array.length(); i++) {

                                    //getting product object from json array
                                    JSONObject product = array.getJSONObject(i);

                                    //adding the product to product list
                                    ticketsList.add(new SelectListTicket(
                                            product.getString("name"),
                                            product.getString("num_travels"),
                                            product.getString("purchase_date"),
                                            product.getString("expiration_date"),
                                            product.getInt("status_tickets_id"),
                                            product.getString("username"),
                                            product.getInt("ticket_id")

                                    ));
                                }
                            }
                            else {
                                Toast.makeText(SelectTicketActivity.this, "Error.", Toast.LENGTH_LONG).show();


                            }

                            //creating adapter object and setting it to recyclerview
                            SelectTicketAdapter adapter = new SelectTicketAdapter(SelectTicketActivity.this, ticketsList);
                            recyclerView.setAdapter(adapter);
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        Volley.newRequestQueue(this).add(stringRequest);
    }

    private void getExtraData() {
        Intent intent = getIntent();
        Bundle d1 = intent.getExtras();
        if(d1 != null) {
            ticket_id = (String)d1.getString("ticket_id");
            username = (String)d1.getString("username");

        }
    }
}
