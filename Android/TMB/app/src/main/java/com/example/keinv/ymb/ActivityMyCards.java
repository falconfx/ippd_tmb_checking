package com.example.keinv.ymb;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class ActivityMyCards extends AppCompatActivity {

    String data;
    RecyclerView recyclerView;
    List<ListTickets> ticketsList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cards);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ticketsList = new ArrayList<>();


        getExtraData();
        loadRecyclerViewData();

    }

    private void loadRecyclerViewData() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cargando Tarjetas...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://35.204.171.13/storage/gettickets.php?name=" + data,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //converting the string to json array object
                            JSONArray array = new JSONArray(response);

                            //traversing through all the object
                             Log.isLoggable("prueba", array.length() );
                            if(array.length() != 0){
                                for (int i = 0; i < array.length(); i++) {

                                    //getting product object from json array
                                    JSONObject product = array.getJSONObject(i);

                                    //adding the product to product list
                                    ticketsList.add(new ListTickets(
                                            product.getInt("ticket_id"),
                                            product.getInt("num"),
                                            product.getString("name"),
                                            product.getString("description"),
                                            product.getString("travels"),
                                            product.getString("username")
                                    ));
                                }
                            }
                            else {
                                Toast.makeText(ActivityMyCards.this, "Todavía no has comprado ninguna tarjeta. Porfavor compra alguna.", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(ActivityMyCards.this, ActivityBuy.class);
                                intent.putExtra("id3", data);
                                startActivity(intent);


                            }

                            //creating adapter object and setting it to recyclerview
                            TicketAdapter adapter = new TicketAdapter(ActivityMyCards.this, ticketsList);
                            recyclerView.setAdapter(adapter);
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        Volley.newRequestQueue(this).add(stringRequest);
    }

    private void getExtraData() {
        Intent intent = getIntent();
        Bundle d1 = intent.getExtras();
        if(d1 != null) { data = (String)d1.getString("id2"); }
    }





}

