package com.example.keinv.ymb;

public class SelectListTicket {

    private int statusTicketsId;
    private String name;
    private String travels;
    private String purchaseDate;
    private String expirationDate;
    private String username;
    private int ticketId;

    public SelectListTicket(  String name, String travels, String purchaseDate, String expirationDate, int statusTicketsId, String username, int ticketId)
    {
        this.statusTicketsId = statusTicketsId;
        this.name = name;
        this.travels = travels;
        this.purchaseDate = purchaseDate;
        this.expirationDate = expirationDate;
        this.username = username;
        this.ticketId = ticketId;


    }

    public int getStatusTicketsId() { return statusTicketsId;}
    public String getName() { return name; }
    public String getTravels() { return travels; }
    public String getPurchaseDate() { return purchaseDate; }
    public String getExpirationDate() { return expirationDate; }
    public String getUsername() { return username; }
    public int getTicketId() { return ticketId; }
}
