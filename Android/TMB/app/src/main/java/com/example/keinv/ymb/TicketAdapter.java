package com.example.keinv.ymb;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;


public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.ProductViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;
    public View view;


    //we are storing all the products in a list
    private List<ListTickets> productList;
    public ListTickets product;

    //getting the context and product list with constructor
    public TicketAdapter(Context mCtx, List<ListTickets> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.list_tickets, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        //getting the product of the specified position
        product = productList.get(position);

        //binding the data with the viewholder views

        holder.textViewName.setText(product.getName());
        holder.textViewNum.setText("Tienes " + product.getNum() + " tarjetas" );
        holder.textViewDescr.setText(product.getDesc());
        holder.textViewTravels.setText("En total tienes "+ product.getTravels() + " viajes");
        holder.textViewId.setText(""+ product.getTicketId());
        holder.textViewUsername.setText(product.getUsername());




    }


    @Override
    public int getItemCount() {
        return productList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView textViewNum, textViewName, textViewDescr, textViewTravels, textViewId, textViewUsername;
        CardView cView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            textViewNum = itemView.findViewById(R.id.textViewNumTicket);
            textViewName = itemView.findViewById(R.id.textViewNameTicket);
            textViewDescr = itemView.findViewById(R.id.textViewDescTicket);
            textViewTravels = itemView.findViewById(R.id.textViewTravelsTicket);
            textViewId = itemView.findViewById(R.id.textViewIdTicket);
            textViewUsername = itemView.findViewById(R.id.textViewUsernameTicket);

            cView = itemView.findViewById(R.id.cardView);

            view = itemView;

            cView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Intent miIntent = new Intent(v.getContext(), SelectTicketActivity.class);
                    miIntent.putExtra("ticket_id", textViewId.getText());
                    miIntent.putExtra("username", textViewUsername.getText());
                    v.getContext().startActivity(miIntent);
                }
            });
        }
    }
}