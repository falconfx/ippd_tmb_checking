package com.example.keinv.ymb;

public class ListTickets {

    private int ticketId;
    private int num;
    private String name;
    private String desc;
    private String travels;
    private String username;

    public ListTickets(int ticketId, int num, String name, String desc, String travels, String username)
    {
        this.ticketId = ticketId;
        this.num = num;
        this.name = name;
        this.desc = desc;
        this.travels = travels;
        this.username = username;

    }


    public int getTicketId() { return ticketId; }
    public int getNum() { return num; }
    public String getName() { return name; }
    public String getDesc() { return desc; }
    public String getTravels() { return travels; }
    public String getUsername() { return username; }
}
