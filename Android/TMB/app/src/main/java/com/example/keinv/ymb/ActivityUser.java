package com.example.keinv.ymb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.widget.Toast;

public class ActivityUser extends AppCompatActivity {
    String datos;
    Button BtnBuy, BtnLocation, BtnMyCards;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        BtnBuy = findViewById(R.id.BtnBuy);
        BtnMyCards = findViewById(R.id.b2);
        BtnLocation = findViewById(R.id.BtnLocation);
        Intent intent = getIntent();
        Bundle d1 = intent.getExtras();

        if(intent.hasExtra("ticketSubtracted"))
        {
            Toast.makeText(ActivityUser.this, "¡Buen viaje! Se ha descontado 1 viaje", Toast.LENGTH_LONG).show();
        }

        datos = (String)d1.getString("id");
        BtnBuy.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent miIntent = new Intent(ActivityUser.this, ActivityBuy.class );
            miIntent.putExtra("id3", datos);
            startActivity(miIntent);
        }
        });
        BtnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent miIntent = new Intent(ActivityUser.this, MapsActivity.class );
                startActivity(miIntent);
            }
        });
        BtnMyCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent miIntent = new Intent(ActivityUser.this, ActivityMyCards.class );
                miIntent.putExtra("id2", datos);
                startActivity(miIntent);
                Intent intent = new Intent(getApplicationContext(), ActivityUser.class);

            }
        });
    }
}