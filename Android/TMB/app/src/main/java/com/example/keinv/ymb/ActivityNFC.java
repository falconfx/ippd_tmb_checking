package com.example.keinv.ymb;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ActivityNFC extends AppCompatActivity {

    String username;
    String ticket_id;
    String selected_ticket_id;
    Intent intent;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);
        getExtraData();

        final ImageView prepareNFC = (ImageView) findViewById(R.id.prepareNFC);
        final ImageView imageNFC = (ImageView) findViewById(R.id.imageNFC);
        final TextView textNFC = (TextView) findViewById(R.id.textNFC);


        final ImageView okNFC = (ImageView) findViewById(R.id.okNFC);
        final ImageView imageOkNFC = (ImageView) findViewById(R.id.imageOkNFC);
        final TextView textOkNFC = (TextView) findViewById(R.id.textOkNFC);

        intent = new Intent(getApplicationContext(), ActivityUser.class);
        intent.putExtra("ticketSubtracted","Buen viaje. Descontado 1 viaje");
        // intent.putExtra("ticket_id", ticket_id);
        intent.putExtra("id", username);

        subtractTravel();


        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                prepareNFC.animate().alpha(0f).setDuration(2000);
                prepareNFC.setClickable(false);
                imageNFC.animate().alpha(0f).setDuration(2000);
                imageNFC.setClickable(false);
                textNFC.animate().alpha(0f).setDuration(2000);
                textNFC.setClickable(false);

                okNFC.animate().alpha(1f).setDuration(2000);
                okNFC.setClickable(false);
                imageOkNFC.animate().alpha(1f).setDuration(2000);
                imageOkNFC.setClickable(false);
                textOkNFC.animate().alpha(1f).setDuration(2000);
                textOkNFC.setClickable(false);


                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        startActivity(intent);
                    } }, 2000);

            }
        }, 2000);



    }

    public void subtractTravel(){
        StringRequest request = new StringRequest(Request.Method.POST, "http://35.204.171.13/storage/subtractTravel.php?name="+username+"&ticket_id="+ticket_id+"&selected_ticket_id="+selected_ticket_id,

                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response){

                        if(response.contains("1"))
                        {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        Volley.newRequestQueue(this).add(request);
    }

    private void getExtraData() {
        Intent intent = getIntent();
        Bundle d1 = intent.getExtras();
        if(d1 != null) {
            ticket_id = (String)d1.getString("ticket_id");
            username = (String)d1.getString("username");
            selected_ticket_id = (String)d1.getString("selected_ticket_id");

        }
    }

}
