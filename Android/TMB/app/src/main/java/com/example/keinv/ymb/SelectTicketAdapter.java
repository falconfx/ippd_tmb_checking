package com.example.keinv.ymb;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;


public class SelectTicketAdapter extends RecyclerView.Adapter<SelectTicketAdapter.ProductViewHolder> {


    //this context we will use to inflate the layout
    private Context mCtx;
    public View view;


    //we are storing all the products in a list
    private List<SelectListTicket> productList;
    public SelectListTicket product;

    //getting the context and product list with constructor
    public SelectTicketAdapter(Context mCtx, List<SelectListTicket> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.select_list_tickets, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        //getting the product of the specified position
        product = productList.get(position);

        //binding the data with the viewholder views

        holder.textViewName.setText(product.getName());
        holder.textViewTravels.setText("Quedan "+ product.getTravels() + " viajes");
        holder.textViewPurchase.setText("Comprada el día " + product.getPurchaseDate());
        holder.textViewExpiration.setText("Caduca el día " + product.getExpirationDate());
        holder.textViewStatusTicketsId.setText(""+ product.getStatusTicketsId());
        holder.textViewUsername.setText(product.getUsername());
        holder.textViewTicketId.setText(""+product.getTicketId());

    }


    @Override
    public int getItemCount() {
        return productList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView textViewStatusTicketsId, textViewName,  textViewTravels, textViewPurchase, textViewExpiration, textViewUsername, textViewTicketId;
        CardView cView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            textViewStatusTicketsId = itemView.findViewById(R.id.textViewSelectedStatusTicketsIdTicket);
            textViewName = itemView.findViewById(R.id.textViewNameSelectTicket);
            textViewTravels = itemView.findViewById(R.id.textViewNumSelectTicket);
            textViewPurchase = itemView.findViewById(R.id.textViewPurchaseSelectTicket);
            textViewExpiration = itemView.findViewById(R.id.textViewExpirationSelectTicket);
            textViewUsername = itemView.findViewById(R.id.textViewSelectedUsernameTicket);
            textViewTicketId = itemView.findViewById(R.id.textViewSelectedTicketId);

            cView = itemView.findViewById(R.id.selectCardView);

            view = itemView;

            cView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Intent miIntent = new Intent(v.getContext(), ActivityNFC.class);
                    miIntent.putExtra("selected_ticket_id", textViewStatusTicketsId.getText());
                    miIntent.putExtra("username", textViewUsername.getText());
                    miIntent.putExtra("ticket_id", textViewTicketId.getText());

                    v.getContext().startActivity(miIntent);
                }
            });
        }
    }
}