package com.example.keinv.ymb;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.HashMap;
import java.util.Map;



public class MainActivity extends AppCompatActivity {

    CardView btn_login;
    EditText et_username,et_pass;
    TextView et_register;
    public static String EXTRA_MESSAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_login = findViewById(R.id.btn_login);
        et_register = findViewById(R.id.register);

        et_username = (EditText) findViewById(R.id.et_username);
        et_pass = (EditText) findViewById(R.id.et_pass);

        btn_login.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                login();
            }
        });

        et_register.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                intent.putExtra(EXTRA_MESSAGE, "reg");
                startActivity(intent);
            }
        });

    }




    public void login(){
        StringRequest request = new StringRequest(Request.Method.POST, "http://35.204.171.13/storage/login.php",
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response){

                        if (response.contains("1")) {
                            Intent intent = new Intent(getApplicationContext(), ActivityUser.class);
                            //String user = et_username.getText().toString();
                            intent.putExtra("id", et_username.getText().toString());
                            startActivity(intent);

                            //startActivity(new Intent(getApplicationContext(),ActivityUser.class));

                        }else {
                            Toast.makeText(getApplicationContext(),"Wrong username or password", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username",et_username.getText().toString());
                params.put("password",et_pass.getText().toString());
                return params;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }



}
